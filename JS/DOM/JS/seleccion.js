document.addEventListener("DOMContentLoaded", inicializacion);

function inicializacion() {
    var cambiaClaseButton = document.getElementById('cambiaClase');
    cambiaClaseButton.addEventListener("click", cambiaEslilosClase);

    var cambiaLiButton = document.getElementById('cambiarLi');
    cambiaLiButton.addEventListener("click", cambiaEslilosLi);
}

function cambiaEslilosClase() {
    var elementos = document.getElementsByClassName('item');
    // console.log(elementos);
    // console.log(elementos.length);
    console.log(elementos.item(0));

    for (let i = 0; i < elementos.length; i++) {
        console.log('item:', elementos[i]);
        elementos.item(i).style.color = '#fff';
    }
}

function cambiaEslilosLi(){
    var liElements = document.getElementsByTagName('li');
    console.log(liElements);

    for (let i = 0; i < liElements.length; i++) {
        // console.log('Item: ', liElements.item(i));
        liElements.item(i).classList.add('itemgrande');
    }
}