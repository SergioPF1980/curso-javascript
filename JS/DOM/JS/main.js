console.log(document.forms[0].action);

// document.forms[0].elements[0].value = 'Hola';
// document.forms[0].campo.value = 'Hola soy un campo';
document.forms.myForm.campo.value = 'Hola, accedo mediante el nombre del formulario';

// document.getElementById('etiqueta-campo').textContent = 'Aqui!!!!!';
document.getElementById('etiqueta-campo').addEventListener('click', cambiaFoco);


function cambiaFoco(){
    document.forms.myForm.campo.focus();
}


document.getElementById('campo').addEventListener('keyup', function(){
    // this => Referencia sobre el objeto donde se ha detectado el evento
    console.log('Lo que hay escrito es: ', this.value);
})